package droidtr.duckdns;

import android.app.Activity;
import droidtr.duckdns.R;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        EditText domain = (EditText) findViewById(R.id.domain);
        EditText token = (EditText) findViewById(R.id.token);
        SharedPreferences reader = getSharedPreferences("data",MODE_PRIVATE);
        domain.setText(reader.getString("domain",""));
        token.setText(reader.getString("token",""));
    }
    public void update (View v){
        EditText domain = (EditText) findViewById(R.id.domain);
        EditText token = (EditText) findViewById(R.id.token);
        WebView w = (WebView)findViewById(R.id.w);
        SharedPreferences.Editor editor = getSharedPreferences("data",MODE_PRIVATE).edit();
        editor.putString("domain",domain.getText().toString());
        editor.putString("token",token.getText().toString());
        editor.commit();
        w.loadData("","","");
        w.loadUrl("https://www.duckdns.org/update?domains="+domain.getText().toString()+"&token="+token.getText().toString()+"&ip=");
        w.setVisibility(View.VISIBLE);
    }
}
